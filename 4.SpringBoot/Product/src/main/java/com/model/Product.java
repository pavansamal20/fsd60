package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity						//Table create
public class Product {
	
	@Id   						// primary key column
	@GeneratedValue   		// to generated the id autoincrement
	
	private int id;
	
	@Column          		    // our own custom column or provide custom column 
	private String name;
	private double price;
	
	
	
	public Product() {
	}
	
	public Product(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	// toString is used for testing purpose ....once to operation is completed we can remove the toString method.
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
}
