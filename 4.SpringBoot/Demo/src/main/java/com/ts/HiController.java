package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {
	
	@RequestMapping("SayHi")
	public String sayHi() {
		return "Hi from Controller!!!";
	}
	
	@RequestMapping("Welcome")
	public String welcome(){
		return "Welcome from HiController";
	}
	
	@RequestMapping("HelloUser")
	public String HelloUser(){
		return " HelloUser from Controller";
	}
}
