package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/HRHomePage")
public class HRHomePage extends HttpServlet {
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		//To Showcase emailId
		//String emailId = request.getParameter("emailId");
		
		//Fetching emailId from the HttpSession Object
		HttpSession session = request.getSession(false);
		String emailId = (String) session.getAttribute("emailId");
		
		out.print("<html>");
		out.print("<body bgcolor='lightyellow'>");
		
		//Providing the Links for Home and Logout
		out.print("<form align='right'>");
		out.print("<a href='HRHomePage'>Home</a> &nbsp;");
		
		//out.print("<a href='Login.html'>Logout</a>");
		//calling Logout Servlet
		out.print("<a href='Logout'>Logout</a>");
		out.print("</form>");
			
		//To Showcase emailId
		out.print("<h3>Welcome: " + emailId + "!</h3>");
		
		out.print("<center>");
		out.print("<h1 style='color:green'>Welcome to HRHomePage</h1>");
		
		//Adding the Link to GetAllEmployees (Servlet)	
		out.print("<h3><a href='GetAllEmployees'>GetAllEmployees</a> &nbsp;&nbsp;");
		
		//Providing the GetEmployeeById Link
		out.print("<a href='GetEmployeeById.html'>GetEmployeeById</a></h3>");
		out.print("<center></body></html>");
			
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
