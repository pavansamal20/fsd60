package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String studentName = request.getParameter("StudentName");
		String course = request.getParameter("course");
		String gender = request.getParameter("gender");
		String mobile = request.getParameter("mobile");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		out.print("<h1>Student Details</h1>");
		out.print("<h3>StudentId   : " + studentId + "</h3>");
		out.print("<h3>StudentName : " + studentName + "</h3>");
		out.print("<h3>Course  : " + course + "</h3>");
		out.print("<h3>Gender  : " + gender + "</h3>");
		out.print("<h3>Mobile  : " + mobile + "</h3>");
		out.print("<h3>Email-Id: " + emailId + "</h3>");
		out.print("<h3>Password: " + password + "</h3>");
		out.print("<center></body></html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}

