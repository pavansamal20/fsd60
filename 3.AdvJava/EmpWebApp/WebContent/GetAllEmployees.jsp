<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>
	<% List<Employee> employeeList = (List<Employee>) request.getAttribute("employeeList"); %>

	<%@ include file="HRHomePage.jsp"%>
	<br />

	<table border='2' align='center'>

		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>Email-Id</th>
		</tr>


		<% for (Employee employee : employeeList) { %>

		<tr>
			<td><%= employee.getEmpId()   %></td>
			<td><%= employee.getEmpName()  %></td>
			<td><%= employee.getSalary()   %></td>
			<td><%= employee.getGender()   %></td>
			<td><%= employee.getEmailId()  %></td>
		</tr>
		<% } %>

	</table>
	
</body>
</html>