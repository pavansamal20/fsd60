package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection connection = DbConnection.getConnection();
        PreparedStatement preparedStatement = null;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Employee ID to delete:");
        int empId = scanner.nextInt();
        System.out.println();

        String deleteQuery = "DELETE FROM employee WHERE empId=?";

        try {
            preparedStatement = connection.prepareStatement(deleteQuery);

            preparedStatement.setInt(1, empId);

            int result = preparedStatement.executeUpdate();

            if (result > 0) {
                System.out.println("Record Deleted...");
            } else {
                System.out.println("Record Deletion Failed!!!");
            }

            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

	}

}
