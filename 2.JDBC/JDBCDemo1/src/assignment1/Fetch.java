package assignment1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

// Fetch operation 
public class Fetch {

	public static void main(String[] args) {
		 Connection connection = DbConnection.getConnection();
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;

	        Scanner scanner = new Scanner(System.in);

	        String selectIdsQuery = "SELECT empId FROM employee";

	        try {
	            preparedStatement = connection.prepareStatement(selectIdsQuery);
	            resultSet = preparedStatement.executeQuery();

	            System.out.println("Employee IDs:");
	            while (resultSet.next()) {
	                System.out.print(resultSet.getInt("empId") + " ");
	            }
	            System.out.println();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        }

	        System.out.println("\nSelect Employee ID:");
	        int selectedEmpId = scanner.nextInt();

	        String selectQuery = "SELECT * FROM employee WHERE empId = ?";

	        try {
	            preparedStatement = connection.prepareStatement(selectQuery);
	            preparedStatement.setInt(1, selectedEmpId);
	            resultSet = preparedStatement.executeQuery();

	            if (resultSet.next()) {
	                System.out.println("Employee ID: " + resultSet.getInt("empId"));
	                System.out.println("Employee Name: " + resultSet.getString("empName"));
	                System.out.println("Salary: " + resultSet.getDouble("salary"));
	                System.out.println("Gender: " + resultSet.getString("gender"));
	                System.out.println("Email ID: " + resultSet.getString("emailId"));
	                System.out.println("Password: " + resultSet.getString("password"));
	            } else {
	                System.out.println("Employee with ID " + selectedEmpId + " not found.");
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                if (resultSet != null) {
	                    resultSet.close();
	                }
	                if (preparedStatement != null) {
	                    preparedStatement.close();
	                }
	                if (connection != null) {
	                    connection.close();
	                }
	                scanner.close(); 
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
