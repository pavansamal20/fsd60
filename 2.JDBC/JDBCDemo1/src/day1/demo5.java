package day1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class demo5 {

	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "select * from employee";
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			
			if(resultSet != null) {
				while(resultSet.next()) {
					System.out.print("EmpId    : " + resultSet.getInt(1)      + " ");
					System.out.print("EmpName  : " + resultSet.getString(2)   + " ");
					System.out.print("Salary   : " + resultSet.getDouble(3)   + " ");
					System.out.print("Gender   : " + resultSet.getString(4)   + " ");
					System.out.print("EmailId  : " + resultSet.getString(5)   + " ");
					System.out.print("Password : " + resultSet.getString(6)   + " ");
					System.out.println("\n");
				}
				
			} else {
				System.out.println("No Record(s) Found!!! ");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
