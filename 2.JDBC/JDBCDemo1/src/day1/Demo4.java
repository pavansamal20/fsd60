package day1;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo4 {

	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId: ");
		int empId = scanner.nextInt();
		
		String deleteQuery = "delete from employee where empId = " + empId;
		
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(deleteQuery);
			
			if (result > 0) {
				System.out.println(result + "Record(s) Deleted.... ");
			} else {
				System.out.println("Record Deletion Failed... ");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try{
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		
	}

}
