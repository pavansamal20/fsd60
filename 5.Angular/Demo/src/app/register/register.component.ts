import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  emp: any;
  countries: any;
  departments: any;

  constructor(private router: Router, private service: EmpService) {

    //Making the emp object in such a way that it contains the department json object
    this.emp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId:''
      }
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { 
      console.log(data);
      this.departments = data; 
    });
  }

  empRegister(regForm: any) {

    //Binding the registerForm data to the emp object, as it contains the department as a Json object
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;   
    
    console.log(this.emp);
    
    this.service.registerEmplooyee(this.emp).subscribe((data: any) => { console.log(data); });

    alert("Employee Registered Successfully!!!");
    this.router.navigate(['login']);
}

}




