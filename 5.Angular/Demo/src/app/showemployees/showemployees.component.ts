import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {

  employees: any;
  emailId: any;

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      console.log(data);
      this.employees = data;
    });
  }

  editEmployee(emp: any) {
    alert('Edit Employee, empId: ' + emp.empId);
  }

  deleteEmployee(empId:any) {
    this.service.deleteEmployee(empId).subscribe((data: any) => {console.log(data);});
  
    const i = this.employees.findIndex((element: any) => {
      return empId == element.empId;
    });

    this.employees.splice(i, 1);  
  }

}
