import { Component,OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {

  isUserLogged: any;

  constructor(private service: any) {
  }

  ngOnInit() {
    this.service.getIsUserLogged().subscribe((data: any) => {
      this.isUserLogged = data;
    });
  }

}
